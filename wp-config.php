<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tmdt');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z+9(qvUa*.gX!J^5Kf&9g$yG;%{Pu:Q%_fK%e>}?G?(MK1#|%).ER4T5yUv5.%Gk');
define('SECURE_AUTH_KEY',  'x)nrUk_y1Y*PyG,ctIakq=o8Y3A&,PR+9V,37b/h;xf:l96{``ZAh-9a:qDSWPSn');
define('LOGGED_IN_KEY',    'nlIX[,Jt#6c;Z%|74o8I(mobwJ/%-5e:K(x/hD}B_,i>2XV{?{)`1L G,y<fxYb,');
define('NONCE_KEY',        ':j<B&$r%73.)RN7XPN,9UL1Y$%;*--E`(7&.m@[LPHonf?]sb[f!o.(ayiEjVtC;');
define('AUTH_SALT',        '4pn.DK-tFFigVQi~;$}syxA {)_ ;l]EGB!:pmjI%o-|Hu2wN]l93:FJ=h6u@ ]:');
define('SECURE_AUTH_SALT', '#.^aj>P?gfSAlN)8_#/lYbV)_QY>45z4>-?DV#M%l<,&)6h8Z)&6_PiFrp9)%iV&');
define('LOGGED_IN_SALT',   'cM?jE_SAosS_r5p8-#6;Vt*I{]5+:EMB~JvBXW`VZ`O$]/t[%G`}Ek~%_$U*Sbn4');
define('NONCE_SALT',       ':FM-et?@l?QN$j hNu}zKYVD6UtBC0rd9KDJ(_eLS:lFH8g*W.!S_$=!5sW=omvy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
